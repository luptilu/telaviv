import React, { Component }  from 'react';
import { AppRegistry, SectionList, StyleSheet, Text, View, Button } from 'react-native';
import PlacesConfig from '../utils/PlacesConfig';

class PlaceInfo extends React.Component { 
  constructor(props) {
    super(props);

    this.state = {
      descriptionVisible : false
    }
  }

  render() {
    return (
      <View>
        <Button
          onPress={() => this.setState({descriptionVisible : !this.state.descriptionVisible})}
          title={this.props.title}/>
        {this.state.descriptionVisible &&
          <Text>{this.props.description}</Text> 
        }  
      </View>
    )
  }
}

export default class Places extends React.Component {
    render() {
        let sections = Object.keys(PlacesConfig)
          .map(key => {
            
            let data = PlacesConfig[key].items
              .map(item => ({name : item.name, description : item.description + "            " + item.address}));

            return {title : key, data : data};
          })

        // [
        //   {title : 'restaunrant', data : {name : .., description: ..}  }
        //   {title : 'restaunrant', }
        //   ...
        //   {title : 'bars', }
        //   ..

        // ]
        
        return (
            <View style={styles.container}>
            <SectionList
              sections={sections}
              renderItem={({item}) => 
                <PlaceInfo
                  title={item.name}
                  description={item.description} 
                  style={styles.item}/>}
              renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
              keyExtractor={(item, index) => index}
            />
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
      container: {
       flex: 1,
       paddingTop: 22
      },
      sectionHeader: {
        paddingTop: 2,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 2,
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        backgroundColor: '#EB8D72',
      },
      item: {
        padding: 10,
        fontSize: 18,
        height: 44,
        backgroundColor: '#EB8D72',
      },
    })
    