import React from 'react';
import Mapbox from '@mapbox/react-native-mapbox-gl';

import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Modal,
  TouchableOpacity,
} from 'react-native';

import { Icon } from 'react-native-elements';

// styles
import sheet from '../styles/sheet';
import colors from '../styles/colors';

// utils
import { IS_ANDROID } from '../utils';
import config from '../utils/config';
import configPlaces from '../utils/PlacesConfig';

const styles = StyleSheet.create({
  noPermissionsText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  header: {
    marginTop: 48,
    fontSize: 24,
    textAlign: 'center',
  },
  exampleList: {
    flex: 1,
    marginTop: 60 + 12, // header + list padding,
  },
  exampleListItemBorder: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#ccc',
  },
  exampleListItem: {
    paddingVertical: 32,
    paddingHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  exampleListLabel: {
    fontSize: 18,
  },
  exampleBackground: {
    flex: 1,
    backgroundColor: colors.primary.pinkFaint,
  },
  annotationContainer: {
    width: 10,
    height: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    borderRadius: 15,
  },
  annotationIcon: {
    width: 10,
    height: 10,
    borderRadius: 1,
    transform: [{ scale: 0.7 }],
  }
});

export default class Map extends React.Component {

  async componentWillMount() {
    if (IS_ANDROID) {
      const isGranted = await Mapbox.requestAndroidLocationPermissions();
      this.setState({
        isAndroidPermissionGranted: isGranted,
        isFetchingAndroidPermission: false,
      });
    }
  }
  renderAnnotations() {
    return Object.keys(configPlaces)
      .map(key => {
        let annotation = configPlaces[key];
        return annotation.items.map(item => {
          return (
            <Mapbox.PointAnnotation
              key={`${key}_${item.name}`}
              id={`${key}_${item.name}`}
              coordinate={item.coordinates}>

              <View>
                <Icon 
                  name={annotation.icon} 
                  size={25}
                  type='font-awesome'
                  color={annotation.color}/>
              </View>
              <Mapbox.Callout title={item.name}/>
            </Mapbox.PointAnnotation>);
        });
      })
  }

  render() {
    if (IS_ANDROID && !this.state.isAndroidPermissionGranted) {
      if (this.state.isFetchingAndroidPermission) {
        return null;
      }
      return (
        <View style={sheet.matchParent}>
          <Text style={styles.noPermissionsText}>
            You need to accept location permissions in order to use this example
            applications
          </Text>
        </View>
      );
    }

    return (
      <View style={sheet.matchParent}>
        <Mapbox.MapView
          centerCoordinate={[34.773078, 32.063069]}
          showUserLocation
          zoomLevel={11}
          userTrackingMode={Mapbox.UserTrackingModes.Follow}
          styleURL="mapbox://styles/luptilu/cjdvk13ra0tp72rnwjwnuanv3"
          style={sheet.matchParent}
          attributionEnabled>
          {this.renderAnnotations()}
        </Mapbox.MapView>
      </View>
    );
  }
}